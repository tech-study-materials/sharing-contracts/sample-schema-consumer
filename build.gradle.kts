
plugins {
    java
//    id("org.ajoberstar.grgit") version "5.0.0-rc.3"
}

tasks.compileJava {
    options.release.set(17)
}

repositories {
    mavenCentral()
}

val initSchemas by tasks.registering(Exec::class) {
    group = "build"
    doFirst{ println("fetching schemas") }
    commandLine("git", "submodule", "update", "--recursive", "--init")
}

val pullSchemas by tasks.registering(Exec::class) {
    group = "build"
    doFirst{ println("pulling newest schemas, don't forget to commit if new version arrived") }
    commandLine("git", "submodule", "update", "--recursive", "--remote")
}

val schemaVersionsToGenerateCodeFrom = listOf("v3", "v4")
val generateSourcesFromSchema by tasks.registering {
    group = "build"
    dependsOn(initSchemas, pullSchemas)
    doLast {
        schemaVersionsToGenerateCodeFrom.forEach { version ->
            val path = "schemas/$version/schema.txt"
            println("Generating sources from $path (replace this task with the actual generator)...")
            println(File(path).readText())
        }
    }
}

tasks.compileJava {
    dependsOn(generateSourcesFromSchema)
}