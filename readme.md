Demo:

* `git clone https://gitlab.com/tech-study-materials/sharing-contracts/sample-schema-consumer.git`
* `cd sample-schema-consumer`
* `gradlew build`

---

This repo uses `schemas` repo as submodule. 
* It means that after `git clone` && `gradlew initSchemas` you will see a /schemas subdir in project directory, with content of `schemas` repo
* You don't need to invoke `gradlew initSchemas` explicitly, it will be invoked automatically during `gradlew build`

It was achieved by:

`git submodule add https://gitlab.com/tech-study-materials/sharing-contracts/schemas.git`
resulting in adding `.gitmodules` file in root dir

**`schemas` subdirectory should be treated as read-only** - developers shall not change anything inside it directly, 
but rather apply changes in the original `schemas` repo and pull incoming changes via gradle `pullSchemas` task.


What is expected to happen during development and CI/CD:
* compilation task depends on `generateSourcesFromSchema`, which depends on `pullSchemas`
* therefore calling `gradle build` should:
  * pull/update schemas, putting them in `schemas` subdirectory
  * `generateSourcesFromSchema` has now access to local copies of the schema files, so can generate code
* Disclaimer: git will use keys/credentials of the user working with this repo to clone `schemas` repo, so the user needs to have proper permissions